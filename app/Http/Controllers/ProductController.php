<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    function list(Request $request){
        $search = $request -> input("search");
        $kolom = $request -> input("kolom");
        $urutan = $request -> input("urutan");
        $btn = $request -> input("btn");

        if ($btn == '1' && $search != "" && $kolom == "" && $urutan == "") {
            $data_product = Product::where("name", "like", "%".$search."%")
                                    -> orderBy("id", "asc")
                                    ->paginate(5);
        } elseif ($btn == '1'  && $kolom != "" && $urutan != "" && $search == "") {
            $data_product = Product::orderBy($kolom, $urutan)
                                    ->paginate(5);
        } elseif ($btn == '1' && $kolom != "" && $urutan == "" && $search == "")  {
            $data_product = Product::orderBy($kolom, "asc")
                                    ->paginate(5);
        } elseif ($btn == '1' && $kolom == "" && $urutan != "" && $search == "")  {
            $data_product = Product::orderBy("id", $urutan)
                                    ->paginate(5);
        } else {
            $data_product = Product::paginate(5);
        }

        // $data_product = Product::query();
        // if ($kolom != ""  && $urutan != "") {
        //     $data_product = $data_product -> orderBy($kolom, $urutan);
        // }

        // if ($kolom != ""  && $urutan == "") {
        //     $data_product = $data_product -> orderBy($kolom, "asc");
        // }

        // if ($kolom == ""  && $urutan != "") {
        //     $data_product = $data_product -> orderBy("id", $urutan);
        // }

        // if ($search != "") {
        //     $data_product = $data_product -> where("name", "like", "%".$search."%");
        // }

        // $data_product = $data_product -> paginate(5);
        return view('product-list')
            -> with("data_product", $data_product);
    }

    function create(){
        return view('product-create');
    }

    function save(Request $request){
        $data_product = Product::create([
            "name" => $request -> input("product_name"),
            "kode" => $request -> input("kode"),
            "kuant" => $request -> input("kuantitas"),
            "status" => $request -> input("status")
        ]);

        if($data_product) {
            return redirect(url("product"))
                ->with("status", "Sukses memproses.");
        } else {
            return redirect(url("product"))
                ->with("status", "Gagal memproses.");
        }
    }

    function edit($id) {
        $data_product = Product::find($id);
        return view('product-edit')
            ->with("data_product", $data_product);
    }

    function update($id, Request $request) {
        $data_product = Product::find($id);
        $data_product -> name = $request -> input("product_name");
        $data_product -> kode = $request -> input("kode");
        $data_product -> kuant = $request -> input("kuantitas");
        $data_product -> status = $request -> input("status");
        $data_product -> save();
        return redirect(url("product"));
    }

    function delete($id) {
        $data_product = Product::find($id);
        $data_product -> delete();
        return redirect(url('product'));
    }
}
