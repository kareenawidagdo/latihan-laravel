<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    function list(Request $request){
        $search = $request -> input("search");
        $kolom = $request -> input("kolom");
        $urutan = $request -> input("urutan");
        $data_category = Category::query();
        if ($kolom != ""  && $urutan != "") {
            $data_category = $data_category -> orderBy($kolom, $urutan);
        }

        if ($kolom != ""  && $urutan == "") {
            $data_category = $data_category -> orderBy($kolom, "asc");
        }

        if ($kolom == ""  && $urutan != "") {
            $data_category = $data_category -> orderBy("id", $urutan);
        }

        if ($search != "") {
            $data_category = $data_category -> where("name", "like", "%".$search."%");
        }

        $data_category = $data_category -> paginate(5);
        return view('category-list')
            -> with("data_category", $data_category);
    }

    function create(){
        return view('category-create');
    }

    function save(Request $request){
        $data_category = Category::create([
            "name" => $request -> input("category_name"),
            "slug" => $request -> input("slug"),
            "menu" => $request -> input("menu_order"),
            "status" => $request -> input("status")
        ]);

        if($data_category) {
            return redirect(url("category"))
                ->with("status", "Sukses memproses.");
        } else {
            return redirect(url("category"))
                ->with("status", "Gagal memproses.");
        }
    }

    function edit($id) {
        $data_category = Category::find($id);
        return view('category-edit')
            ->with("data_category", $data_category);
    }

    function update($id, Request $request) {
        $data_category = Category::find($id);
        $data_category -> name = $request -> input("category_name");
        $data_category -> slug = $request -> input("slug");
        $data_category -> menu = $request -> input("menu_order");
        $data_category -> status = $request -> input("status");
        $data_category -> save();
        return redirect(url("category"));
    }

    function delete($id) {
        $data_category = Category::find($id);
        $data_category -> delete();
        return redirect(url('category'));
    }
}
