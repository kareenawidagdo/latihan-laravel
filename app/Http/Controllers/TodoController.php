<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TodoController extends Controller
{
    function list() {
        $data_todo = Todo::all();
        return view('todo-list')
            -> with("data_todo", $data_todo);
    }
}
