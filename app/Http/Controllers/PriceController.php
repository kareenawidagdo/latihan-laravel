<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PriceController extends Controller
{
    function list(){
        $data_price = Price::all();
        return view('price-list')
            -> with("data_price", $data_price);
    }
}
