<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PKL Hari Pertama</title>
    <style>
        .container {
            margin-top: 5%;
            margin-left: 6%;
        }
        .btn {
            background-color: white;
            border: none;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            font-size: 16px;
            border-radius:2.5em;
        }
        .btn:hover {
            background-color: #ECEAE4;
            border: none;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            font-size: 16px;
            border-radius:2.5em;
            font-weight: bold;
        }
        .container-2 {
            margin-left: 2%;
            font-family: Segoe UI;
            color: #2F4F4F;
        }
        a {
            text-decoration: none;
        }
    </style>
</head>
<body bgcolor="#B0E0E6">
    <div class="container">
        <div class="mt-3">
            <a href="/product">
                <button class="btn"><b>Product</b></button>
            </a>
            &ensp;
            <a href="/category">
                <button class="btn"><b>Category</b></button>
            </a>
            &ensp;
            <a href="/todo">
                <button class="btn"><b>To Do</b></button>
            </a>
            &ensp;
            <a href="/price">
                <button class="btn"><b>Prices</b></button>
            </a>
            &ensp;
            <a href="/new-list">
                <button class="btn"><b>Create</b></button>
            </a>
            <br>
        </div>
        <div class="container-2">
            <p>
                Halo!<br>
                Mari coba klik tombol diatas ini yang akan mengarahkanmu ke halaman lain.
            </p>
            <p>Atina</p>
        </div>
    </div>
</body>
</html>