<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create New List</title>
    <style>
        .container {
            margin-top: 5%;
            margin-left: 6%;
            font-family: Segoe UI;
        }
        .btn {
            background-color: white;
            border: none;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            font-size: 16px;
            border-radius:2.5em;
            width: 190px;
        }
        .btn:hover {
            background-color: #ECEAE4;
            border: none;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            font-size: 16px;
            border-radius:2.5em;
            font-weight: bold;
        }
        a {
            text-decoration: none;
        }
    </style>
</head>
<body bgcolor="#B0E0E6">
    <div class="container">
        <p style="color: #2F4F4F; font-size: 20pt;">
            Pilih salah satu untuk menambah data.
        </p> <br>
    </div>
    <div class="wadah-btn">
        <center>
            <a href="/product/create">
                <button class="btn"><b>Create Product</b></button>
            </a>
            <br><br>
            <a href="/category/create">
                <button class="btn"><b>Create Category</b></button>
            </a>
            <br><br>
            <a href="/todo/create">
                <button class="btn"><b>Create To Do</b></button>
            </a>
            <br><br>
            <a href="/price/create">
                <button class="btn"><b>Create Price</b></button>
            </a>
            <br><br>
            <a href="/">
                <button class="btn"><b>Back to Home</b></button>
            </a>
        <center>
    </div>
</body>
</html>