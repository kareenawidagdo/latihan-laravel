<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Category</title>
    <link rel="icon" href="favicon.ico">
    <style>
        .form-baru {
            font-family: Segoe UI;
            border: 3px solid #4B0082;
            padding: 10px 15px;
        }
        .garis {
            border: 3px solid #4B0082;
            padding: 10px;
            width: 300px;
        }
        .add {
            font-size: 30px;
            font-weight: bolder;
            color: red;
            background-color: white;
        }
        .btn {
            background-color: black;
            border: none;
            padding: 7px 10px;
            color: white;
            text-align: center;
            text-decoration: none;
            font-size: 12px;
            border-radius:0.5em;
        }
        body {
            margin-left: 39%;
            margin-top: 2.5%;
        }
        .teks {
            font-size: 11pt;
        }
        input[type="text"] {
            width: 250px;
            height: 20px;
            border: none;
        }
    </style>
</head>
<body bgcolor="#E6E6FA">
    <div class="garis">
        <center>
            <p class="add">Edit Category</p>
        </center>
        <div class="form-baru">
            <form action="/category/update/{{$data_category->id}}" method="POST">
                @csrf
                <p>
                    <label class="teks">Nama Kategori</label><br>
                    <input type="text" name="category_name" value="{{$data_category->name}}"><br>
                </p>
                <p>
                    <label class="teks">Slug</label><br>
                    <input type="text" name="slug" value="{{$data_category->slug}}"><br>
                </p>
                <p>
                    <label class="teks">Urutan</label><br>
                    <input type="text" name="menu_order" value="{{$data_category->menu}}"><br>
                </p>
                <p>
                    <label class="teks">Status</label><br>
                    <input type="text" name="status" value="{{$data_category->status}}"><br>
                </p>
                <p>
                    <input type="submit" name="tombol-add" value="Edit" class="btn">
                </p>
            </form>
        </div>
    </div>
</body>
</html>