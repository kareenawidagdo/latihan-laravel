<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Category List</title>
    <style>
        .container {
            margin-top: 2%;
            margin-left: 6%;
        }
        .category {
            color: red;
        }
        tbody {
            color: #2F4F4F;
        }
        table {
            font-family: Segoe UI;
        }
        .back {
            background-color: black;
            border: none;
            padding: 10px;
            color: white;
            text-align: center;
            text-decoration: none;
            font-size: 12px;
            border-radius:0.5em;
        }
        .btn {
            margin-left: 92%;
        }
        a {
            text-decoration: none;
        }
        .btn-2 {
            border: none;
            background-color: red;
            color: white;
            border-radius: 0.1em;
            font-weight: bold;
            width: 60px;
            height: 30px;
        }
        .btn-3 {
            border: none;
            background-color: green;
            color: white;
            border-radius: 0.1em;
            font-weight: bold;
            width: 60px;
            height: 30px;
        }
        .search-box {
            width: 500px;
            border: none;
            padding: 10px;
        }
        .search {
            font-size: 12pt;
            height: 36px;
            width: 36px;
            border: none;
        }
        .angka {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }
        .opt-1 {
            width: 200px;
            border: none;
            padding: 10px;
        }
        .opt-2 {
            width: 170px;
            border: none;
            padding: 10px;
        }
    </style>
</head>
<body bgcolor="#B0E0E6">
    <div class="btn">
        <p>
            <a href="{{url('/')}}">
                <button class="back">Back to Home</button>
            </a>
        </p>
    </div>
    <div class="container">
        <form method="get" action="{{url('category')}}">
            <table>
                <tr>
                    <td>Search :</td>
                    <td>Kolom :</td>
                    <td>Urutan :</td>
                    <td></td>
                </tr>
                <tr>
                    <td><input type="text" name="search" placeholder="Search here..." class="search-box"></td>
                    <td>
                        <select name="kolom" class="opt-1">
                            <option></option>
                            <option name="name" value="name">Nama Kategori</option>
                            <option name="menu" value="menu">Urutan</option>
                        </select>
                    </td>
                    <td>
                        <select name="urutan" class="opt-2">
                            <option></option>
                            <option name="asc" value="asc">ASC</option>
                            <option name="desc" value="desc">DESC</option>
                        </select>
                    </td>
                    <td>
                        <button type="submit" class="search"><i class="fa fa-search"></i></button>
                    </td>
                </tr>
            <table>
        </form>
        <h1 class="category">Category List</h1>
        <table border="1" cellspacing="0" cellpadding="10">
            <thead bgcolor="white">
                <tr>
                    <th>ID</th>
                    <th width="200px">Nama Kategori</th>
                    <th width="300px">Slug</th>
                    <th width="50px">Urutan</th>
                    <th width="100px">Status</th>
                    <th>Option</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data_category as $row)
                <tr>
                    <td style="text-align: center;">{{$row->id}}</td>
                    <td>{{$row->name}}</td>
                    <td>{{$row->slug}}</td>
                    <td style="text-align: center;">{{$row->menu}}</td>
                    <td style="text-align: center;">{{$row->status}}</td>
                    <td>
                        <a href="/category/edit/{{$row->id}}">
                            <button class="btn-2">Edit</button>
                        </a>
                        <a href="/category/delete/{{$row->id}}">
                            <button class="btn-3">Delete</button>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>