<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('product', 'ProductController@list');

Route::get('category', 'CategoryController@list');

Route::get('todo', 'TodoController@list');

Route::get('price', 'PriceController@list');

Route::get('new-list', 'NewController@show');

Route::get("category/create", "CategoryController@create");

Route::post("category/save", "CategoryController@save");

Route::get("category/edit/{id}", "CategoryController@edit");

Route::post("category/update/{id}", "CategoryController@update");

Route::get("category/delete/{id}", "CategoryController@delete");

Route::get("product/create", "ProductController@create");

Route::post("product/save", "ProductController@save");

Route::get("product/edit/{id}", "ProductController@edit");

Route::post("product/update/{id}", "ProductController@update");

Route::get("product/delete/{id}", "ProductController@delete");